// @flow

import * as React from 'react'
import { Platform, StatusBar } from 'react-native'
import { connect } from 'react-redux'

import HomeView from '../../screens/Home'
import { fetchPictures } from './actions'
import type { UnsplashImage } from '../../services/UnsplashAPI'

export type Props = {
  navigation: any,
  fetchPictures: Function,
  pictures: Array<UnsplashImage>,
  isLoading: boolean,
  page: number,
}

class HomeContainer extends React.Component<Props, void> {
  static navigationOptions = {
    header: null,
  }

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor('#000')
  }

  componentDidMount () {
    this.onRefresh(true)
  }

  onRefresh = (shouldEnforce?: boolean): void => {
    if (!this.props.isLoading || shouldEnforce) {
      this.props.fetchPictures(1)
    }
  }

  onLoadNext = (): void =>  {
    if (!this.props.isLoading) {
      this.props.fetchPictures(this.props.page + 1)
    }
  }

  render () {
    return <HomeView {...this.props}
      onRefresh={this.onRefresh}
      onLoadNext={this.onLoadNext} />
  }
}

function bindAction (dispatch) {
  return {
    fetchPictures: page => dispatch(fetchPictures(page)),
  }
}

const mapStateToProps = state => ({
  pictures: Object.values(state.homeReducer.pictures),
  page: state.homeReducer.page,
  isLoading: state.homeReducer.isLoading,
})

export default connect(mapStateToProps, bindAction)(HomeContainer)

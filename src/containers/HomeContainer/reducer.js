// @flow

import {
  PICTURES_FETCH_REQUESTED,
  PICTURES_FETCH_SUCCESS,
  FETCH_FAILED,
} from './actions'

const initialState = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload

  switch (action.type) {
    case PICTURES_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true
      }

    case PICTURES_FETCH_SUCCESS:
      const { pictures, page } = payload
      const currentPictures = page === 1 ? {} : state.pictures
      const newPictures = pictures.reduce((dictionary, picture) => ({
        ...dictionary,
        [picture.id]: picture,
      }), {...currentPictures})

      return {
        ...state,
        isLoading: false,
        pictures: newPictures,
        page
      }

    case FETCH_FAILED:
      const { errorMessage } = payload

      return {
        ...state,
        isLoading: false,
        errorMessage
      }

    default:
      return state
  }
}

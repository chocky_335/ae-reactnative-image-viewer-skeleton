// @flow

import { getPictures } from '../../services/UnsplashAPI'
import type { ActionWithPayload, ActionWithoutPayload, Dispatch } from '../../types/actions'

export const PICTURES_FETCH_REQUESTED = 'PICTURES_FETCH_REQUESTED'
export const PICTURES_FETCH_SUCCESS = 'PICTURES_FETCH_SUCCESS'
export const FETCH_FAILED = 'FETCH_FAILED'


export function listIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURES_FETCH_REQUESTED,
  }
}

export function fetchListSuccess (pictures: Array<Object>, page: number): ActionWithPayload {
  return {
    type: PICTURES_FETCH_SUCCESS,
    payload: {
      page,
      pictures
    }
  }
}

export function fetchListFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    payload: {
      errorMessage
    }
  }
}

export function fetchPictures (page: number = 1) {
  return async (dispatch: Dispatch) => {
    dispatch(listIsLoading())

    getPictures(page)
      .then(photos => { dispatch(fetchListSuccess(photos, page)) })
      .catch(err => { dispatch(fetchListFailed(err)) })
  }
}

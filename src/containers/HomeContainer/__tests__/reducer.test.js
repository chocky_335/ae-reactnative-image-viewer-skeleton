import reducer from '../reducer'

import {
  PICTURES_FETCH_REQUESTED,
  PICTURES_FETCH_SUCCESS,
  FETCH_FAILED,
} from '../actions'

const defaultState = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
}

describe('list reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState)
  })

  it('should trigger isLoading property', () => {
    const state = {
      ...defaultState,
      isLoading: false
    }
    const action = {
      type: PICTURES_FETCH_REQUESTED,
      payload: {
        page: 1
      }
    }

    expect(reducer(undefined, {})).toEqual({
      ...defaultState,
      isLoading: true
    })
  })
})

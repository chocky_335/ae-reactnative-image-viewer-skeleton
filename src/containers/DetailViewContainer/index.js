// @flow
import * as React from 'react'
import { Share } from 'react-native'
import DetailView from '../../screens/DetailView'
import { connect } from 'react-redux'

export type Props = {|
  navigation: any,
|}
export type Filter = 'Blur' | null
export type State = {|
  filter: Filter,
|}

export const filters: Filter[] = ['Blur', null]

const defaultState: Filter = null

class DetailViewContainer extends React.Component<Props, State> {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: 'transparent',
      position: 'absolute',
      height: 50,
      top: 0,
      left: 0,
      right: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: '#FFF',
  }

  state = {
    filter: defaultState,
  }

  share = (): void => {
    const { pictureDetails } = this.props.navigation.state.params

    const imageUrl = pictureDetails.urls.full
    const message = pictureDetails.description || ''

    Share.share({ message: imageUrl })
  }

  applyFilter = (): void => {
    const { filter } = this.state
    const indexOfCurrent = filters.indexOf(filter)

    let nextFilter = filter === null
      ? filters[0]
      : filters[indexOfCurrent + 1]

    this.setState({ filter: nextFilter })
  }

  render () {
    const { pictureDetails } = this.props.navigation.state.params

    const imageUrl = pictureDetails.urls.full
    const thumbUrl = pictureDetails.urls.thumb

    return <DetailView
      filter={this.state.filter}
      imageUrl={imageUrl}
      thumbUrl={thumbUrl}
      pictureDetails={pictureDetails}
      shareCallback={this.share}
      changeFilterCallback={this.applyFilter}
    />
  }
}

export default DetailViewContainer

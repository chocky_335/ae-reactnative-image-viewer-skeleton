// @flow
import * as React from 'react'
import {
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native'
import styles from '../styles'
import imageFiltersImage from './images/ImageFilters.png'
import shareImage from './images/ShareThis.png'

type Props = {|
  firstName: ?string,
  lastName: ?string,
  date: string,
|}

const parseNames = (fName: ?string, lName: ?string) => {
  if (fName && !lName) {
    return fName
  } else if (!fName && lName) {
    return lName
  } else if (fName && lName) {
    return `${fName.slice(0, 1)}. ${lName}`
  }

  return 'unknown'
}

const PhotoDetails = ({ firstName, lastName, date }: Props) => (
  <View>
    <Text style={styles.detailTitle}>{parseNames(firstName, lastName)}</Text>
    <Text style={styles.detailSubtitle}>{`${date.slice(0, 10)}`}</Text>
  </View>
)

export default PhotoDetails

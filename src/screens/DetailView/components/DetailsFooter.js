// @flow

import * as React from 'react'
import {
  TouchableOpacity,
  Image,
  View,
} from 'react-native'

import styles from '../styles'

import imageFiltersImage from './images/ImageFilters.png'
import PhotoDetails from './PhotoDetails'

import shareImage from './images/ShareThis.png'

import type { UnsplashImage } from '../../../services/UnsplashAPI'


type Props = {|
  shareCallback: () => void,
  changeFilter: () => void,
  pictureDetails: UnsplashImage,
|}

class DetailsFooter extends React.PureComponent<Props> {
  render () {
    const { shareCallback, changeFilter, pictureDetails } = this.props

    if (!pictureDetails) {
      return null
    }

    const imageId = pictureDetails.id
    const { user } = pictureDetails

    return (
      <View style={styles.detailView}>
        <PhotoDetails
          firstName={user.first_name}
          lastName={user.last_name}
          date={pictureDetails.created_at}
        />

        <View style={styles.detailSubContainer}>
          <TouchableOpacity
            style={{marginRight: 10}}
            onPress={changeFilter}
          >
            <Image style={styles.detailViewImage}
              resizeMode='cover'
              source={imageFiltersImage} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{alignSelf: 'flex-end'}}
            onPress={shareCallback}
          >
            <Image style={styles.detailViewImage}
              resizeMode='cover'
              source={shareImage} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default DetailsFooter

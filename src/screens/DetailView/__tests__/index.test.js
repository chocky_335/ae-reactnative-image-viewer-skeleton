import React from 'react'
import DetailView from '../index'

import renderer from 'react-test-renderer'

import type { UnsplashImage } from '../../../services/UnsplashAPI'

const applyFilterCallback = jest.fn()
const shareCallback = jest.fn()
const imageUrl = 'https://drscdn.500px.org/photo/247377659/m%3D900_k%3D1_a%3D1/v2?client_application_id=27071&webp=true&sig=b5b4070666461be985e7890d9c3b26054ae52c8c882fe9b6120bb2c627b53204'

const imgDetails: UnsplashImage = {
  id: 'test',
  created_at: '2018-09-15T14:29:55-04:00',
  user: {
    first_name: 'firstName',
    last_name: 'lastName'
  },
  urls: {
    full: imageUrl,
    regular: imageUrl,
    thumb: imageUrl,
  }
}


describe('renders correctly', () => {
  it('without filters', () => {
    const tree = renderer.create(
      <DetailView
        imageUrl={imageUrl}
        thumbUrl={imageUrl}
        applyFilterCallback={applyFilterCallback}
        shareCallback={shareCallback}
        pictureDetails={imgDetails}
        filter={null}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('with filter `Blur`', () => {
    const filter = 'Blur'

    const tree = renderer.create(
      <DetailView
        imageUrl={imageUrl}
        thumbUrl={imageUrl}
        applyFilterCallback={applyFilterCallback}
        shareCallback={shareCallback}
        pictureDetails={imgDetails}
        filter={filter}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
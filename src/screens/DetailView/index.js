// @flow
import * as React from 'react'
import {
  ActivityIndicator,
  View,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Dimensions
} from 'react-native'
import Image from 'react-native-image-progress'

import styles from './styles'

import DetailsFooter from './components/DetailsFooter'
import { filters } from '../../containers/DetailViewContainer/index'

import type { Filter } from '../../containers/DetailViewContainer/index'
import type { UnsplashImage } from '../../services/UnsplashAPI'


const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window')

type Props = {|
  imageUrl: string,
  thumbUrl: string,
  shareCallback: () => void,
  changeFilterCallback: () => void,
  pictureDetails: UnsplashImage,
  filter: ?Filter,
  doAnimateZoomReset: boolean,
  maximumZoomScale: number,
  minimumZoomScale: number,
  zoomHeight: number,
  zoomWidth: number,
|}

class DetailView extends React.PureComponent<Props> {
  static defaultProps = {
    doAnimateZoomReset: false,
    maximumZoomScale: 2,
    minimumZoomScale: 1,
    zoomHeight: deviceHeight,
    zoomWidth: deviceWidth,
  }

  zoomRef: any = null
  scrollResponderRef: any = null

  handleResetZoomScale = () => {
    this.scrollResponderRef.scrollResponderZoomTo({
      x: 0,
      y: 0,
      width: this.props.zoomWidth,
      height: this.props.zoomHeight,
      animated: true
    })
  }

  setZoomRef = (node: any) => {
    if (node) {
      this.zoomRef = node
      this.scrollResponderRef = this.zoomRef.getScrollResponder()
    }
  }

  getFilterProps = () => {
    const { filter } = this.props
    switch (filter) {
      case filters[0]:
        return {
          blurRadius: 1
        }

      default:
        return {}
    }
  }

  render () {
    const { imageUrl, thumbUrl, shareCallback, changeFilterCallback, pictureDetails } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
        <ScrollView
          contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}
          centerContent
          maximumZoomScale={this.props.maximumZoomScale}
          minimumZoomScale={this.props.minimumZoomScale}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          ref={this.setZoomRef}
          style={{ overflow: 'hidden' }}
        >
          <TouchableOpacity
            onPress={this.handleResetZoomScale}
            activeOpacity={1}
          >

              <ImageBackground style={styles.imageStyle}
                resizeMethod='scale'
                resizeMode='cover'
                source={{uri: thumbUrl}}
              >
                <Image style={styles.imageStyle}
                  resizeMethod='scale'
                  resizeMode='cover'
                  source={{uri: imageUrl}}
                  indicator={() => <ActivityIndicator size='large' color='#fff' />}
                  {...this.getFilterProps()}
                />
              </ImageBackground>
          </TouchableOpacity>
        </ScrollView>
        </View>

        <DetailsFooter
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          changeFilter={changeFilterCallback}
        />
      </View>
    )
  }
}

export default DetailView

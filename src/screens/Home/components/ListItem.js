import * as React from 'react'
import {
  ActivityIndicator,
  TouchableOpacity,
  ImageBackground,
} from 'react-native'
import Image from 'react-native-image-progress'

import styles from '../styles'
import type { UnsplashImage } from '../../../services/UnsplashAPI'

type Props = {|
  imageUrl: string,
  thumbUrl: string,
  imageId: number,
  openPicture: (id: string) => void,
  imageStyle: UnsplashImage,
|}

class ListItem extends React.PureComponent<Props> {
  render () {
    const { thumbUrl, imageUrl, imageId, openPicture, imageStyle } = this.props
    return (
      <TouchableOpacity
        onPress={() => openPicture(imageId)}
        style={styles.item}
        delayPressIn={200}>
        <ImageBackground style={imageStyle}
          resizeMethod='scale'
          resizeMode='cover'
          source={{uri: thumbUrl}}
        >
          <Image style={imageStyle}
            resizeMethod='scale'
            resizeMode='cover'
            source={{uri: imageUrl}}
            indicator={() => <ActivityIndicator size='large' color='#fff' />}
          />
        </ImageBackground>
      </TouchableOpacity>
    )
  }
}

export default ListItem

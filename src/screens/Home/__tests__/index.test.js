import React from 'react'
import HomeView from '../index'

import renderer from 'react-test-renderer'

import type { UnsplashImage } from '../../../services/UnsplashAPI'

const onRefresh = jest.fn()
const onLoadNext = jest.fn()
const fetchPictures = jest.fn()
const imageUrl = 'https://drscdn.500px.org/photo/247377659/m%3D900_k%3D1_a%3D1/v2?client_application_id=27071&webp=true&sig=b5b4070666461be985e7890d9c3b26054ae52c8c882fe9b6120bb2c627b53204'

const imgDetails: UnsplashImage = {
  id: 'test',
  created_at: '2018-09-15T14:29:55-04:00',
  user: {
    first_name: 'firstName',
    last_name: 'lastName'
  },
  urls: {
    full: imageUrl,
    regular: imageUrl,
    thumb: imageUrl,
  }
}
const images = [imgDetails]


describe('renders correctly', () => {
  describe('without images', () => {
    it('with loader', () => {
      const tree = renderer.create(
        <HomeView
          onRefresh={onRefresh}
          onLoadNext={onLoadNext}
          navigation={{}}
          fetchPictures={fetchPictures}
          pictures={[]}
          page={1}
          isLoading
        />
      ).toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  describe('with images', () => {
    it('with loader', () => {
      const tree = renderer.create(
        <HomeView
          onRefresh={onRefresh}
          onLoadNext={onLoadNext}
          navigation={{}}
          fetchPictures={fetchPictures}
          pictures={images}
          page={1}
          isLoading
        />
      ).toJSON()
      expect(tree).toMatchSnapshot()
    })
    it('without loader', () => {
      const tree = renderer.create(
        <HomeView
          onRefresh={onRefresh}
          onLoadNext={onLoadNext}
          navigation={{}}
          fetchPictures={fetchPictures}
          pictures={images}
          page={1}
          isLoading={false}
        />
      ).toJSON()
      expect(tree).toMatchSnapshot()
    })
  })
})
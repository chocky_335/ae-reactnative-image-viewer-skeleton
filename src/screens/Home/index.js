// @flow
import * as React from 'react'
import {
  FlatList,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  View,
} from 'react-native'

import ListItem from './components/ListItem'

import styles from './styles'

import type { Props as CommonProps } from '../../containers/HomeContainer/index'


type Props = CommonProps & {
  onRefresh: () => void,
  onLoadNext: () => void,
}

const keyExtractor = (item, page) => item.id

class HomeView extends React.PureComponent<Props, void> {
  imageThumbnailStylePortrait = null

  constructor (props: Props) {
    super(props)
    this._prepareStyles()
  }

  _prepareStyles (): void {
    const { height, width } = Dimensions.get('window')
    const realWidth = height > width ? width : height
    const portraitImageSize = realWidth / 2 - 10
    this.imageThumbnailStylePortrait = StyleSheet.flatten({
      width: portraitImageSize,
      height: portraitImageSize,
      backgroundColor: 'rgba(255,255,255,0.1)'
    })
  }

  _openPicture = (imageId: number): void => {
    const { pictures, navigation } = this.props
    navigation.navigate('DetailView', {
      pictureDetails: pictures.find(pic => pic.id === imageId)
    })
  }

  _renderPicture = ({ item }) => {
    const imageURL = item.urls.regular
    const thumbUrl = item.urls.thumb
    const imageId = item.id
    return <ListItem
      thumbUrl={thumbUrl}
      imageUrl={imageURL}
      imageId={imageId}
      imageStyle={this.imageThumbnailStylePortrait}
      openPicture={this._openPicture}
    />
  }

  _renderList () {
    const { isLoading, page, pictures, onLoadNext, onRefresh } = this.props

    if (pictures.length === 0) {
      return <ActivityIndicator size='large' color='#fff' />
    }

    return <FlatList
      removeClippedSubviews
      refreshing={isLoading}
      initialNumToRender={12}
      data={pictures.slice()}
      onRefresh={onRefresh}
      numColumns={2}
      renderItem={this._renderPicture}
      keyExtractor={(item) => keyExtractor(item, page)}
      onEndReached={this.props.onLoadNext}
      onEndThreshold={2}
    />
  }

  render () {
    const { isLoading, pictures } = this.props

    return <View style={styles.page}>
      {this._renderList()}
    </View>
  }
}

export default HomeView

// @flow

import Unsplash, { toJson } from 'unsplash-js'


const ACCESS_KEY = 'bd1e6fd5b19281036a6c53a2e7586afd11b8d374d78e307fe27ad0b6d12036f1'
const SECRET_KEY = 'e664cc2ab12cd4a3260f38cb7ec61d3012f117566e72e8f2ecbd4b49643fcb6a'

const AMOUNT_PHOTOS_PER_PAGE = 30


const unsplashInstance = new Unsplash({
  applicationId: ACCESS_KEY,
  secret: SECRET_KEY,
  callbackUrl: 'urn:ietf:wg:oauth:2.0:oob'
})


export type UnsplashImage = {
  id: string,
  created_at: string,
  user: {
    first_name: ?string,
    last_name: ?string,
  },
  urls: {
    regular: string,
    full: string,
    thumb: string,
  },
}


export const getPictures = async (page: number = 1):Promise<UnsplashImage[]>  =>
  await unsplashInstance
    .photos
    .listPhotos(page, AMOUNT_PHOTOS_PER_PAGE, 'latest')
    .then(toJson)

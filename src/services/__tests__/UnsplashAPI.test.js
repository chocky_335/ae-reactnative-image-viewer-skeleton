import { getPictures } from '../UnsplashAPI'

export type UnsplashImage = {
  id: string,
  created_at: string,
  user: {
    first_name: ?string,
    last_name: ?string,
  },
  urls: {
    regular: string,
    full: string,
    thumb: string,
  },
}

describe('UnsplashAPI', async () => {
  test('the data should be an array of objects', async () => {
    expect.assertions(1)
    const images = await getPictures()

    expect(typeof images[0]).toBe('object')
  })

  test('element should be UnsplashImage', async () => {
    const images = await getPictures()

    expect(typeof images[0]).toBe('object')

    images.forEach(({ user, urls, id, created_at }) => {
      expect(typeof id).toBe('string')
      expect(typeof created_at).toBe('string')
      expect(typeof user.first_name).toBe('string')
      expect(typeof user.last_name).toBe('string')
      expect(typeof urls.full).toBe('string')
      expect(typeof urls.regular).toBe('string')
      expect(typeof urls.thumb).toBe('string')
    })
  })
})

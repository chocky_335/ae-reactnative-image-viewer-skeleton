// @flow
export type ActionWithPayload = {
  type: string,
  payload: Object,
}

export type ActionWithoutPayload = {
  type: string,
}

export type Dispatch = (action: ActionWithPayload | ActionWithoutPayload) => any;

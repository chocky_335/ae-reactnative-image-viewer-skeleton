# AE500pxProject app #

### How do I get set up? ###

#### Required software ####

* [Node.js](https://nodejs.org/en/) Node 8.3 or newer
* [React Native CLI](https://facebook.github.io/react-native/docs/getting-started.html) with its dependencies for Native Code development
* [XCode](https://developer.apple.com/xcode/) with CLI tools
* [Android Studio](https://developer.android.com/studio/index.html)
* [Yarn](https://yarnpkg.com/en/)

#### Install dependencies ####

* install dependencies with `yarn`

#### Run IOS ####

* run with `react-native run-ios`

#### Run Android ####

* bootstrap the emulator
* run with `react-native run-android`

### Testing ###

* run tests with `yarn test`

### Type checking ###

* run the type checker with `yarn flow`
